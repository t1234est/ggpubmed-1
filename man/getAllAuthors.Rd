% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/ggplot_functions.R
\name{getAllAuthors}
\alias{getAllAuthors}
\title{Get all authors of given publications despite their position and display a bar plot of frequencies}
\usage{
getAllAuthors(item_data, max_results = 25)
}
\arguments{
\item{item_data}{A list object containing the pubmed query result from getItemData()}
}
\value{
a bar plot
}
\description{
Get all authors of given publications despite their position and display a bar plot of frequencies
}
\examples{
getItemData("your search term") \%>\% getAllAuthors()
}

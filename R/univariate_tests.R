#' Perform certain univariate tests, such as chi2, fisher, G-test, t-test, and Wilcoxon test for multiple variables of a data frame
#'
#' @param data A data frame or tibble containing the variables to be tested. The last variable must be a kind of group or outcome variable
#' @param tab A data frame or tibble containing already performed test results in broom::tidy() format. For default, an empty tibble is used.
#' @param test A one-letter string describing the actual test to be performed. Possible values are: "g" for DescTools::GTest(), "c" for chisq.test(), "f", for fisher.test(), "w" for wilcox.test(), and "t", for t.test()
#'
#' @return A tibble containg htest object results in broom::tidy format
#' @export
#' @import dplyr broom DescTools
#'
#' @examples mtcars %>% select(mpg, disp, hp, wt, vs) %>% multiUnivarTest(test = "t")
multiUnivarTest <- function(data, tab = tibble(variable = character()), test = "g") {
  num_cols = ncol(data) # find number of columns
  names_data = data %>%
    select(1:(num_cols - 1)) %>%
    names() # create a vector containing all variable names minus the last (outcome)
  tab <- tab
  for (i in 1:(num_cols - 1)) {
    temp = data %>% select(test.var = i, group.var = num_cols)
    switch(test,
           "c" = {temp2 = temp %>% ftable() %>% chisq.test() %>% tidy()},
           "g" = {temp2 = temp %>% ftable() %>% GTest() %>% tidy()},
           "f" = {temp2 = temp %>% ftable() %>% fisher.test() %>% tidy()},
           "w" = {temp2 = temp %>% wilcox.test(test.var ~ group.var, data = .) %>% tidy()},
           "t" = {temp2 = temp %>% t.test(test.var ~ group.var, data = .) %>% tidy()}
    )
    tab <- bind_rows(tab, temp2) # add each test result as a row to the result tibble
  }
  tab$variable = names_data # add the variable names as a dedicated identity column
  return(tab)
}

#' Creates a data frame containing all test results on a per-line basis.
#' Similar to broom::tidy()
#' @param x A vector containing the data to be tested
#'
#' @return A tibble of five common tests for normality: Shapiro Wilk, Shapiro Francia, Anderson Darling, Cramer von Mises, Lilliefors (Kolmogorov-Smirnov)
#' @export
#' @import dplyr nortest
#'
#' @examples mtcars %>% select(mpg) %>% unlist %>% normalityTests()
#' @examples mtcars %>% select(mpg) %>% unlist %>% normalityTests() %>% knitr::kable()
normalityTests <- function(x) {
  sw <- shapiro.test(x) # Shapiro Wilk
  sf <- sf.test(x)      # Shapiro Francia
  ks <- lillie.test(x)  # Liliefors Kolmogorov-Smirnov
  ad <- ad.test(x)      # Anderson Darling
  cvm <- cvm.test(x)    # Cramer-von-Mises

  tibble(method = c(sw$method,
                    sf$method,
                    ad$method,
                    cvm$method,
                    ks$method
                    ),
         statistic = names(c(sw$statistic,
                             sf$statistic,
                             ad$statistic,
                             cvm$statistic,
                             ks$statistic
                             )
                           ),
         value = c(sw$statistic,
                   sf$statistic,
                   ad$statistic,
                   cvm$statistic,
                   ks$statistic
                   ),
         sig. = c(sw$p.value,
                  sf$p.value,
                  ad$p.value,
                  cvm$p.value,
                  ks$p.value
                  )
         )
}

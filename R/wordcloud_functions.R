#' Query a search term and plot a wordcloud from author frequency
#'
#' @param item_data A list object containing the pubmed query result from getItemData()
#'
#' @return a wordcloud plot
#' @export
#' @import tm dplyr RColorBrewer tibble
#'
#' @examples getItemData("your search term") %>% wcAuthByKey()
wcAuthByKey <- function(item_data) {
  df <- item_data[2] %>%
    table(dnn = "all_authors") %>%
    as.tibble() %>%
    filter(n > 2)
  wordcloud::wordcloud(df$all_authors, df$n, scale = c(3, 0.3),
            min.freq = 2,
            max.words = 100, random.order = FALSE,
            rot.per = 0.35, use.r.layout = FALSE, colors = brewer.pal(8, "Dark2"))
}

remStopWords <- function(abstracts, rmNum = TRUE,
                         tolw = TRUE, toup = FALSE,
                         rmWords = TRUE, yrWords = NULL,
                         stemDoc = FALSE) {
  abstTxt <- Corpus(VectorSource(abstracts))
  text2.corpus = tm_map(abstTxt, removePunctuation)
  if (rmNum == TRUE) {
    text2.corpus = tm_map(text2.corpus, function(x) removeNumbers(x))
  }
  if (tolw == TRUE) {
    text2.corpus = tm_map(text2.corpus, tolower)
  }
  if (toup == TRUE) {
    text2.corpus = tm_map(text2.corpus, toupper)
  }
  if (rmWords == TRUE) {
    text2.corpus = tm_map(text2.corpus, removeWords, stopwords("english"))
    if (!is.null(yrWords)) {
      text2.corpus = tm_map(text2.corpus, removeWords,
                            yrWords)
    }
  }
  if (stemDoc == TRUE) {
    text2.corpus = tm_map(text2.corpus, stemDocument)
  }
  text2.corpus <- tm_map(text2.corpus, PlainTextDocument)
  tdm <- TermDocumentMatrix(text2.corpus)
  m <- as.matrix(tdm)
  v <- sort(rowSums(m), decreasing = TRUE)
  d <- data.frame(word = names(v), freq = v)
  return(d)
}

no.terms <- c("patient", "patients", "study", "compared", "significant",
              "significantly", "clinical", "respectively", "percent")

#' Query a search term and plot a wordcloud from abstract word frequencies
#'
#' @param item_data A list object containing the pubmed query result from getItemData()
#'
#' @return a wordcloud plot
#' @export
#' @import tm dplyr
#'
#' @examples getItemData("your search term") %>% wcAbstractByKey()
wcAbstractByKey <- function(item_data) {
  wc_data <- item_data[[1]] %>%
    select(abstract) %>%
    remStopWords() %>%
    filter(freq > 2)
  wordcloud::wordcloud(wc_data$word, wc_data$freq, scale = c(3, 0.3),
            min.freq = 2,
            max.words = 100, random.order = FALSE,
            rot.per = 0.35, use.r.layout = FALSE, colors = brewer.pal(8, "Dark2"))
}
